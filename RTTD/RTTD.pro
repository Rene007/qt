#-------------------------------------------------
#
# Project created by QtCreator 2016-10-02T13:17:08
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = RTTD
TEMPLATE = app


SOURCES += main.cpp\
        rttd.cpp \
    qcustomplot.cpp \
    observation.cpp \
    file.cpp \
    settings.cpp \
    constants.cpp \
    simulation.cpp \
    plot.cpp \
    coreutils.cpp \
    expandedplot.cpp \
    automode_settings.cpp

HEADERS  += rttd.h \
    qcustomplot.h \
    observation.h \
    file.h \
    settings.h \
    simulation.h \
    plot.h \
    coreutils.h \
    configurations.h \
    expandedplot.h \
    automode_settings.h

FORMS    += rttd.ui \
    settings.ui \
    simulation.ui \
    expandedplot.ui \
    automode_settings.ui

RESOURCES += \
    images.qrc

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../libpcap-1.8.1/release/ -lpcap
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../libpcap-1.8.1/debug/ -lpcap
else:unix: LIBS += -L$$PWD/../libpcap-1.8.1/ -lpcap

INCLUDEPATH += $$PWD/../libpcap-1.8.1
DEPENDPATH += $$PWD/../libpcap-1.8.1

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../libpcap-1.8.1/release/libpcap.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../libpcap-1.8.1/debug/libpcap.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../libpcap-1.8.1/release/pcap.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../libpcap-1.8.1/debug/pcap.lib
else:unix: PRE_TARGETDEPS += $$PWD/../libpcap-1.8.1/libpcap.a
