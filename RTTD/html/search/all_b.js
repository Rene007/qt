var searchData=
[
  ['observation',['Observation',['../class_observation.html',1,'Observation'],['../class_observation.html#ae37dc80a69d158956d0fbea5dd07f7fa',1,'Observation::Observation(const QVector&lt; double &gt; &amp;data, const double &amp;new_observation)'],['../class_observation.html#acf0d003324dc953be41ff85929072228',1,'Observation::Observation(const double &amp;new_observation, const double &amp;old_mean, const double &amp;old_variance, const int &amp;data_size)'],['../class_plot.html#a0279e66765d145c7bcb2bf9cd471bde4',1,'Plot::Observation()']]],
  ['oleftx',['OLEFTX',['../struct_p_d_f_constants.html#ab4749dcc49ba55e327d2cfdfff3ec4d6',1,'PDFConstants']]],
  ['orange',['ORANGE',['../struct_colors.html#a67786c910c7999dabd69c4dcc8d2025c',1,'Colors']]],
  ['orightx',['ORIGHTX',['../struct_p_d_f_constants.html#af62580622294ade5730d7779814f9078',1,'PDFConstants']]]
];
