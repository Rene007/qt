#include "observation.h"

Observation::Observation(const QVector<double>& data, const double &new_observation)
{
    this->old_data = data;
    this->new_data = data;
    this->new_data.push_back(new_observation);
    this->new_observation = new_observation;
    this->old_mean = CoreUtils::mean(data);
    this->old_variance = CoreUtils::variance(data);
    this->old_standard_deviation = CoreUtils::std_dev(data);
    this->data_size = data.size();
}

Observation::Observation(const double &new_observation, const double &old_mean, const double &old_variance, const int &data_size)
{
    this->old_data = this->new_data;
    this->new_data.push_back(new_observation);
    this->new_observation = new_observation;
    this->old_mean = old_mean;
    this->old_variance = old_variance;
    this->old_standard_deviation = qPow(old_variance,0.5);
    this->data_size = data_size;
}

QVector<double> Observation::getOldData()
{
    return this->old_data;
}

QVector<double> Observation::getNewData()
{
    return this->new_data;
}

double Observation::Mean()
{
    this->new_mean = (this->new_observation + this->data_size*this->old_mean) / (this->data_size + 1);
    return this->new_mean;
}

double Observation::StandardDeviation()
{
    this->new_standard_deviation = qPow(this->Variance(), 0.5);
    return this->new_standard_deviation;
}

double Observation::Variance()
{
    double top1 = (this->new_observation - this->Mean())*(this->new_observation - this->old_mean);
    double top2 = (this->data_size - 1)*this->old_variance;
    this->new_variance = (top1 + top2) / (this->data_size);
    return this->new_variance;
}
