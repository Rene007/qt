#include "expandedplot.h"
#include "ui_expandedplot.h"

ExpandedPlot::ExpandedPlot(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ExpandedPlot)
{
    ui->setupUi(this);
    plot.reset(new Plot(ui->observationPlot));
    plot->setTitle("New Observation", 30);
}

ExpandedPlot::~ExpandedPlot()
{
    delete ui;
}

void ExpandedPlot::setConstants(const PDFConstants &constants)
{
    plot->setConstants(constants);
}

void ExpandedPlot::PlotObservation(const int &plot_index, const double &mu, const double &sigma, const QColor &color, const std::string &type)
{
    plot->setThresholdLabel(ui->Observation_Threshold);
    plot->Observation(plot_index, mu, sigma, color, type);
}
