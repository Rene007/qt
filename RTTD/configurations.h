#ifndef CONFIGURATIONS_H
#define CONFIGURATIONS_H
#include <QColor>
#include <QHash>

/**
 * @brief The ImpactConstants struct saves all constants for the Impact Graph
 * @see
 * @ref Plot::Impact and CoreUtils::impactIntercept for a general idea of how the impact constants are used.
 */
struct ImpactConstants {
    double R1 = 10;     /**< R1 is the impact constant with a positive exponential loss. */
    double R2 = 1;      /**< R2 is the impact constant with a negative exponential loss. */
    double LOSS = 50;   /**< LOSS is the impact loss. */
};

/**
 * @brief The Colors struct saves all colors used throughout this module
 */
struct Colors {
    QColor BLUE = QColor(40, 110, 255);     /**< Blue color */
    QColor ORANGE = QColor(255, 110, 40);   /**< Orange color */
    QColor BLACK = QColor(0,0,0);           /**< Black color */
    QColor PURPLE = QColor(186,85,211);     /**< Puprle color */
    QColor RED = QColor(255,0,0);           /**< Red color */
};

/**
 * @brief The Decisions struct saves different decision labels depending
 * on how the decision the module makes based on P(s1|x) and P(s2|x)
 * @see @ref CoreUtils::decisionFunction to understand how the decisions are
 * being made.
 */
struct Decisions {
    QString ATTACK = "Attack";                  /**< Message displayed when an attack is detected. */
    QString NOATTACK = "No Attack";             /**< Message displayed when no attack is detected. */
    QString WARNING = "Out of Range Traffic";   /**< Message displayed when packet round trip time delay is out of range. */
};

/**
 * @brief The Probabilities struct contains the probability constants for the model.
 */
struct Probabilities {
    double PROB_S1 = 0.99; /**< Initial value for the probability of getting good round trip delay (No Attack). */
    double PROB_S2 = 0.01; /**< Initial value for the probability of getting bad round trip delay (Attack). */
};

/**
 * @brief The PDFConstants struct contains constants related to all probability distribution graphs
 */
struct PDFConstants {
    //For Valid Range
    double MIN_OBSERVATION = 30;    /**< (%) The minimum percentage value of the mean a new observation is allowed to be. */

    double STD_DEV_RANGE = 2.5;     /**< All observations are allowed to be only within
                                         [mean - STD_DEV_RANGE*standard_deviation, mean - STD_DEV_RANGE*standard_deviation]  */

    //For PDF Plot
    double LEFTX = RAND_MAX;        /**< The minimum x value on the x-axis is initialized to be as big as possible (LEFTX)
                                         when plotting "PDF" (see @ref Plot::PDF) so when mean - z_score*standard_deviation is
                                         less than LEFTX, we recalibrate the graph's x-axis. */

    double RIGHTX = -1* RAND_MAX;   /**< The max x value on the x-axis is initialized to be as small as possible (RIGHTX)
                                         when plotting "PDF" (see @ref Plot::PDF) so when mean - z_score*standard_deviation is
                                         greater than RIGHTX, we recalibrate the graph's x-axis. */

    double PERCENT_SHIFT = 0.10;    /**< Percentage the shifted_mean (mean2) is away from the original mean (mean1) */

    double SHIFTED_MEAN_Z_SCORE = 3;/**< Number of standard devition shifted_mean (mean2) is away from original mean (mean1) */

    //For Observation Plot
    double OLEFTX = RAND_MAX;       /**< The minimum x value on the x-axis is initialized to be as big as possible (OLEFTX)
                                         when plotting "Observation" (see @ref Plot::Observation) so when mean - z_score*standard_deviation
                                         is less than OLEFTX, we recalibrate the graph's x-axis. */

    double ORIGHTX = -1* RAND_MAX;  /**< The max x value on the x-axis is initialized to be as small as possible (ORIGHTX)
                                         when plotting "Observation" (see @ref Plot::Observation) so when mean - z_score*standard_deviation
                                         is greater than ORIGHTX, we recalibrate the graph's x-axis. */

    bool isRadio_Std_Dev = true;    /**< Check if Standard Deviation Method is checked (see @ref PDFConstants::SHIFTED_MEAN_Z_SCORE) */

    bool isRadio_Percentage = false;/**< Check if Percentage Method is checked (see @ref PDFConstants::SHIFTED_MEAN_Z_SCORE) */

    bool isDecision1 = true;        /**< Check if Decision 1 Method is checked. In that case,
                                         Ps(s2|x) > P(s1|x) is the main decision criteria (see @ref CoreUtils::decisionFunction)*/

    bool isDecision2 = false;        /**< Check if Decision 2 Method is checked. In that case,
                                         Ps(s2|x) > P(s1|x) (AND logic)  (x > TRHo) is the main decision criteria (see @ref CoreUtils::decisionFunction)*/

    bool isDecision3 = false;        /**< Check if Decision 3 Method is checked. In that case,
                                         Ps(s2|x) > P(s1|x) (AND logic)  (x > TRHo) is the main decision criteria (see @ref CoreUtils::decisionFunction)*/
};

struct AutoMode {
    double MIN = 40;        /**< Minimum value for randomly generated observation */
    double MAX = 50;        /**< Maximum value for randomly generated observation */
    double IAT = 100;       /**< Inter Arrival Time betweeen observations */
    double MULTIPLIER = 10; /**< Multiply observation by this constant in order to slow down automode simulation (see @ref Simulation::simulate)*/
    bool STATUS = false;    /**< Check if automode is on or off. True if on, false if off */
    double observation = 0; /** <Automode observation value */
    int TYPE = 0;        /** <Type of observation. 0 for good packets, 1 for bad packets, 2 for out of range packets */
};

#endif // CONFIGURATIONS_H
