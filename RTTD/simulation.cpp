#include "simulation.h"
#include "ui_simulation.h"
#include <QTimer>
#include <QDebug>

int packetOrignalPos = 175;
int packetFinalPos = 550;
int range = packetFinalPos - packetOrignalPos;
int packetStep = 10;
bool isForward = true;
bool done = false;
int TYPE = 0;   // Packet type (0 for good, 1 for bad, 2 for out of range)

static int packetPosition = packetOrignalPos;
AutoMode automode_constants;
static QTimer *mytimer = new QTimer();

Simulation::Simulation(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Simulation)
{
    ui->setupUi(this);
    connect(mytimer, SIGNAL(timeout()),this,SLOT(movePacket()));
}

Simulation::~Simulation()
{
    delete ui;
}

void Simulation::simulate(const AutoMode &automode_constants)
{
    qInfo() <<"TYPE: "<<automode_constants.TYPE;
    TYPE = automode_constants.TYPE;

    if (automode_constants.STATUS) {
        double observation_time = automode_constants.observation * automode_constants.MULTIPLIER;
        int loop_time = (int) (packetStep*(observation_time/range));
        mytimer->start(loop_time);
    } else {
        mytimer->stop();
    }
}

void Simulation::updatePacketsCount(const QHash<QString, int> &packets_count)
{
    ui->good_packets->setText(QString::number(packets_count["good"]));
    ui->bad_packets->setText(QString::number(packets_count["bad"]));
    ui->out_of_range_packets->setText(QString::number(packets_count["out_of_range"]));
}

void Simulation::movePacket()
{
    int y = ui->packet->y();
    int width = ui->packet->width();
    int height = ui->packet->height();

    if (packetPosition > packetFinalPos) {
        packetPosition = packetFinalPos;
        isForward = false;
        //done = 2;
    }
    if (packetPosition < packetOrignalPos) {
        packetPosition = packetOrignalPos;
        isForward = true;
        done = true;
    }

    ui->packet->setGeometry(packetPosition, y, width, height);
    changePacketColorByType(ui->packet, TYPE);

    if (isForward)
        packetPosition += packetStep;
    else
        packetPosition -= packetStep;

    if(done) {
        mytimer->stop();
        done = false;
    }
}

void Simulation::changePacketColorByType(QLabel *packet_object, int type)
{
    switch(type) {
        case 0:
            changePacketColor(ui->packet,"green");
            break;
        case 1:
            changePacketColor(ui->packet,"red");
            break;
        case 2:
            changePacketColor(ui->packet,"rgb(153, 102, 51)");
            break;
        default:
            changePacketColor(ui->packet,"green");
            break;
    }
}

void Simulation::changePacketColor(QLabel *packet_object, QString color)
{
    packet_object->setAutoFillBackground(true);
    packet_object->setStyleSheet(  "background-color: "+color+";\
                                    border-style: solid;\
                                    border-width:1px; \
                                    border-radius:5px; \
                                    border-color: "+color+"; \
                                    max-width:10px; \
                                    max-height:10px; \
                                    min-width:10px; \
                                    min-height:10px;");
}
