#include "file.h"

File::File(const QString &name)
{
    this->file_name = name;
}

void File::SaveReadingValues(const QHash<QString, double>& params, const QVector<QHash<QString, double>>& data)
{
    QFile file(this->file_name);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream out(&file);

    out << "Min: ," << params["min"] << "\n";
    out << "Max: ," << params["max"] << "\n";
    out << "Observations: ," << params["num_of_observations"] << "\n";
    out <<"\n\n";

    out <<" ,";
    out <<"Reading,";
    out <<"Mean1,";
    out <<"Var1,";
    out <<"Thr1,";
    out <<"Loss,";
    out <<" ,";
    out <<"Mean2,";
    out <<"Var2,";
    out <<" ,";
    out <<"P(s1),";
    out <<"P(x|s1),";
    out <<"P(s1|x),";
    out <<" ,";
    out <<"P(s2),";
    out <<"P(x|s2),";
    out <<"P(s2|x),";
    out <<"\n";

    QVector<QHash<QString, double>>::const_iterator outer;

    for (outer = data.constBegin(); outer != data.constEnd(); ++outer){
        out << (*outer)["observation_number"]<< ", ";

        if (!(*outer)["reading"])
            out << " , ";
        else
            out << (*outer)["reading"]<< ", ";

        out << (*outer)["mean1"]<< ", ";
        out << (*outer)["var1"]<< ", ";
        out << (*outer)["thr1"]<< ", ";
        out << (*outer)["loss"]<< ", ";
        out << " , ";
        out << (*outer)["mean2"]<< ", ";
        out << (*outer)["var2"]<< ", ";
        out << " , ";

        if (!(*outer)["p_s1"])
            out << " , ";
        else
            out << (*outer)["p_s1"]<< ", ";

        if (!(*outer)["p_x_given_s1"])
            out << " , ";
        else
            out << (*outer)["p_x_given_s1"]<< ", ";

        if (!(*outer)["p_s1_given_x"])
            out << " , ";
        else
            out << (*outer)["p_s1_given_x"]<< ", ";


        out << " , ";

        if (!(*outer)["p_s2"])
            out << " , ";
        else
            out << (*outer)["p_s2"]<< ", ";

        if (!(*outer)["p_x_given_s2"])
            out << " , ";
        else
            out << (*outer)["p_x_given_s2"]<< ", ";

        if (!(*outer)["p_s2_given_x"])
            out << " , ";
        else
            out << (*outer)["p_s2_given_x"]<< ", ";

        out <<"\n";
    }
    out <<"\n";
}

