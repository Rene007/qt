#include "plot.h"
#include <QDebug>
#include <typeinfo>

Plot::Plot(QCustomPlot *ui_plot)
{
    this->plot = ui_plot;
    this->isTitleSet = 0;
    this->phaseTracerText = new QCPItemText(this->plot);
    curve = new QCPCurve(this->plot->xAxis, this->plot->yAxis);
    groupTracer = new QCPItemTracer(this->plot);
    //graph = this->plot->addGraph();
}

void Plot::setThresholdLabel(QLabel *threshold)
{
    this->threshold_label = threshold;
}

void Plot::setTitle(const QString &name, const int &size) {
    if (!isTitleSet) {
        QCPTextElement *title = new QCPTextElement(this->plot);
        title->setText(name);
        title->setFont(QFont("sans", size, QFont::Bold));
        this->plot->plotLayout()->insertRow(0);
        this->plot->plotLayout()->addElement(title);
    }
    isTitleSet = 1;
}

void Plot::addData(const int &plot_index, const double &FPR, const double &TPR)
{

    this->plot->addGraph();
    this->plot->graph(plot_index)->setPen(QColor(0,0,0));
    this->plot->graph(plot_index)->setLineStyle(QCPGraph::lsNone);
    this->plot->graph(plot_index)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross, 4));
    this->plot->graph(plot_index)->addData(FPR, TPR);

    this->plot->replot();
}

void Plot::addPointLabel(const double &x_val, const double &y_val)
{
    const double offset_x = 0.2;
    const double offset_y = 0.05;
    QString point_label;
    point_label.sprintf("(%f, %f)",x_val,y_val);
    this->phaseTracerText->position->setCoords(x_val + offset_x, y_val - offset_y);
    this->phaseTracerText->setText(point_label);
    this->phaseTracerText->setTextAlignment(Qt::AlignCenter);
    this->phaseTracerText->setColor(Qt::red);
}

void Plot::setConstants(const ImpactConstants &constants)
{
    this->impact_constants = constants;
}

void Plot::setConstants(const PDFConstants &constants)
{
    this->pdf_constants = constants;
}

void Plot::Impact(const std::string &type, const int &plot_index, const double &mu, const QColor &color)
{
    double step = 0.02;
    double xMin = mu - 1;
    double xMax = mu + 1;
    int length = (int) (xMax - xMin) / step;
    QVector<double> x(length), y(length), xs(2), ys(2);

    int sign = (type == "NEGATIVE") ? -1 : 1;
    double impact = (type == "NEGATIVE") ? impact_constants.R2 : impact_constants.R1;

    double old_leftx = xMin;
    double old_rightx = xMax;

    double val = xMin;
    for (int i=0; i<length; ++i) {
      x[i] = val;
      y[i] = impact/(1 + qExp(sign*impact_constants.LOSS*(x[i] - mu)));
      val += step;
    }

     xs[0] = xs[1] = CoreUtils::impactIntercept(mu, impact_constants);
     ys[0] = -1;
     ys[1] = impact_constants.R1 + 1;

    if (old_leftx < pdf_constants.LEFTX)
        pdf_constants.LEFTX = old_leftx;

    if (old_rightx > pdf_constants.RIGHTX)
        pdf_constants.RIGHTX = old_rightx;

    this->plot->addGraph();
    this->plot->graph(plot_index)->setPen(QPen(color));
    if (type == "LINE_INTERCEPT") {

        this->plot->graph(plot_index)->setData(xs, ys);

        QString threshold;
        threshold.sprintf("Optimal_Threshold = %.3f",xs[0]);
        if (this->threshold_label != nullptr)
            this->threshold_label->setText(threshold);

    } else {
        this->plot->graph(plot_index)->setData(x, y);
    }

    this->setTitle("Loss Functions");

    this->plot->xAxis->setLabel("Observation X");
    this->plot->yAxis->setLabel("Impact");
    this->plot->xAxis->setRange(old_leftx, old_rightx);
    this->plot->yAxis->setRange(-1, impact_constants.R1 + 1);
    this->plot->replot();
}

void Plot::ROC(const int &plot_index, const QVector<double> &FPR, const QVector<double> &TPR, const std::string &type)
{
    double step = 0.02;
    double xMin = 0.0;
    double xMax = 1.0;
    int length = (int) (xMax - xMin) / step + 1;
    QVector<double> xs(length), ys(length);

    double val = xMin;
    for (int i=0; i<length; ++i) {
      xs[i] = val;
      ys[i] = val;
      val += step;
    }

    this->graph = this->plot->addGraph();

    if (type == "RANDOM_GUESSING" || FPR.size() == 0 || TPR.size() == 0) {
        this->plot->graph(plot_index)->setData(xs, ys);
    } else {
        QVector<QCPCurveData> curve_data(3);
        QVector<double> x(3), y(3);
        x[0] = 0; x[1] = FPR[FPR.size() - 1 ]; x[2] = 1;
        y[0] = 0; y[1] = TPR[TPR.size() - 1]; y[2] = 1;

        for (int i = 0; i < 3; ++i) {
            curve_data[i] = QCPCurveData(i, x[i], y[i]);
        }
        this->curve->data()->set(curve_data, true);
        this->curve->setPen(QPen(Qt::red));

        groupTracer->setGraph(this->plot->graph(plot_index));
        groupTracer->setGraphKey(x[1]);
        groupTracer->setInterpolating(true);
        groupTracer->setStyle(QCPItemTracer::tsCircle);
        groupTracer->setPen(QPen(Qt::red));
        groupTracer->setBrush(Qt::red);
        groupTracer->setSize(7);

        this->plot->graph(plot_index)->setPen(QColor(0, 0, 0, 255));
        this->plot->graph(plot_index)->setLineStyle(QCPGraph::lsNone);
        this->plot->graph(plot_index)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross, 4));
        this->plot->graph(plot_index)->setData(FPR, TPR);
    }

    this->setTitle("ROC Curve");

    this->plot->xAxis->setLabel("False Positive Rate");
    this->plot->yAxis->setLabel("True Positive Rate");
    this->plot->xAxis->setRange(xMin, xMax);
    this->plot->yAxis->setRange(xMin, xMax);
    this->plot->replot();
}

void Plot::PDF(const int &plot_index, const double &mu, const double &sigma, const QColor &color, const std::string &type)
{
    double xMin = mu - 5*sigma;
    double xMax = mu + 5*sigma;
    double step = 0.02;
    int length = (int) (xMax - xMin) / step;
    QVector<double> x(length), y(length), xs(2), ys(2);

    double old_ymax = CoreUtils::normPDF(mu,mu,sigma);
    double old_leftx = xMin;
    double old_rightx = xMax;

    static double ymax;
    //static int isTitleSet = 0;

    double val = xMin;
    for (int i=0; i<length; ++i) {
      x[i] = val;
      y[i] = CoreUtils::normPDF(x[i],mu,sigma);
      val += step;
    }

    xs[0] = xs[1] = mu;
    ys[0] = 0;
    ys[1] = ymax + 1;

    if (old_ymax > ymax)
        ymax = old_ymax;

    if (old_leftx < pdf_constants.LEFTX)
        pdf_constants.LEFTX = old_leftx;

    if (old_rightx > pdf_constants.RIGHTX)
        pdf_constants.RIGHTX = old_rightx;

    this->plot->addGraph();
    this->plot->graph(plot_index)->setPen(QPen(color));
    if (type == "LINE_INTERCEPT") {
       this->plot->graph(plot_index)->setData(xs, ys);

       QString threshold;
       threshold.sprintf("Optimal_Threshold = %.3f",xs[0]);
       if (this->threshold_label != nullptr)
          this->threshold_label->setText(threshold);

    } else {
      this->plot->graph(plot_index)->setData(x, y);
    }

    this->setTitle("RTTD Likelihood – System Model");

    this->plot->xAxis->setLabel("Observation X");
    this->plot->yAxis->setLabel("PDF");
    this->plot->xAxis->setRange(pdf_constants.LEFTX, pdf_constants.RIGHTX);
    this->plot->yAxis->setRange(0, old_ymax*(1 + pdf_constants.PERCENT_SHIFT));
    this->plot->replot();
}

void Plot::Observation(const int &plot_index, const double &mu, const double &sigma, const QColor &color, const std::string &type)
{
    double xMin = mu - 4*sigma;
    double xMax = mu + 4*sigma;
    double step = 0.02;
    int length = (int) (xMax - xMin) / step;
    QVector<double> x(length), y(length), xs(2), ys(2);

    double old_ymax = CoreUtils::normPDF(mu,mu,sigma);
    double old_leftx = xMin;
    double old_rightx = xMax;

    static double ymax;

    double val = xMin;
    for (int i=0; i<length; ++i) {
      x[i] = val;
      y[i] = CoreUtils::normPDF(x[i],mu,sigma);
      val += step;
    }

    xs[0] = xs[1] = mu;
    ys[0] = 0;
    ys[1] = ymax + 1;

    if (old_ymax > ymax)
        ymax = old_ymax;

    if (old_leftx < pdf_constants.OLEFTX)
        pdf_constants.OLEFTX = old_leftx;

    if (old_rightx > pdf_constants.ORIGHTX)
        pdf_constants.ORIGHTX = old_rightx;

    this->plot->addGraph();
    this->plot->graph(plot_index)->setPen(QPen(color));
    if (type == "LINE_INTERCEPT") {
      this->plot->graph(plot_index)->setData(xs, ys);

      QString threshold;
      threshold.sprintf("Optimal_Threshold = %.3f",xs[0]);
      if (this->threshold_label != nullptr)
         this->threshold_label->setText(threshold);
    } else {
      this->plot->graph(plot_index)->setData(x, y);
    }

    this->setTitle("New Observation");

    this->plot->xAxis->setLabel("Observation X");
    this->plot->yAxis->setLabel("PDF");
    this->plot->xAxis->setRange(pdf_constants.OLEFTX, pdf_constants.ORIGHTX);
    this->plot->yAxis->setRange(0, old_ymax*(1 + pdf_constants.PERCENT_SHIFT));
    this->plot->replot();
}
