#ifndef FILE_H
#define FILE_H
#include <QHash>
#include <QFile>
#include <QVector>
#include <QTextStream>

/**
 * @brief The File class manipulates all file
 * related functionality such as reading data
 * from and saving data to file.
 * @author Rene Midouin
 * @version 1.0
 */
class File
{
    public:
        /**
         * @brief This constrcutor creates an instance of File class
         * @param name The full path name of the file to be created
         */
        File(const QString &name);

        /**
         * @brief SaveReadingValues saves all info related to a new observation into a csv (default) file
         * @param params The original parameters before any new observations were recorded
         * @param data The data generated from a new observation
         * (old_mean, new_mean, expected_threshold, optimal_threshold, etc.)
         */
        void SaveReadingValues(const QHash<QString, double>& params, const QVector<QHash<QString, double>>& data);

    private:
        QString file_name;


};

#endif // FILE_H
