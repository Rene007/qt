#include "automode_settings.h"
#include "ui_automode_settings.h"

AutoMode_Settings::AutoMode_Settings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AutoMode_Settings)
{
    ui->setupUi(this);
    setIAT(ui->horizontalSlider->value());
    connect(ui->horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(setIAT(int)));

}

AutoMode_Settings::~AutoMode_Settings()
{
    delete ui;
}
void AutoMode_Settings::textEdit()
{
    QHash<QString, double> params;
    params["min_observation"] = ui->textMin->toPlainText().toDouble() ;
    params["max_observation"] = ui->textMax->toPlainText().toDouble();
    params["multiplier"] = ui->textMultiplier->toPlainText().toDouble();
    params["IAT"] = ui->IAT->text().toDouble();

    emit this->valuesChanged(params);
}

void AutoMode_Settings::setValue(const double &min, const double &max, const double &multiplier)
{
    ui->textMin->setText(QString::number(min));
    ui->textMax->setText(QString::number(max));
    ui->textMultiplier->setText(QString::number(multiplier));
}

void AutoMode_Settings::setIAT(int IAT)
{
    IAT = (IAT + 1)*100;
    ui->IAT->setText(QString::number(IAT));
}

void AutoMode_Settings::on_apply_clicked()
{
    this->textEdit();
    this->close();
}
