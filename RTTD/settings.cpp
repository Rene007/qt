#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    ui->label_mean2_percentage->hide();
    ui->textEdit_mean2_percentage->hide();
    ui->radioButton_std_dev->setChecked(true);
    ui->radioButton_decision1->setChecked(true);
}

Settings::~Settings()
{
    delete ui;
}

void Settings::textEdit()
{
    QHash<QString, double> params;
    QHash<QString, bool> radioButtons;
    params["R1"] = ui->textEdit_R1->toPlainText().toDouble() ;
    params["R2"] = ui->textEdit_R2->toPlainText().toDouble();
    params["loss"] = ui->textEdit_loss->toPlainText().toDouble();
    params["prob_s1"] = ui->textEdit_prob_s1->toPlainText().toDouble();
    params["prob_s2"] = ui->textEdit_prob_s2->toPlainText().toDouble();
    params["min_observation"] = ui->textEdit_min_observation->toPlainText().toDouble();
    params["std_dev_range"] = ui->textEdit_std_dev->toPlainText().toDouble();
    params["mean2_zscore"] = ui->textEdit_mean2_zscore->toPlainText().toDouble();
    params["mean2_percentage"] = ui->textEdit_mean2_percentage->toPlainText().toDouble();
    radioButtons["mean2_zscore"] = ui->radioButton_std_dev->isChecked();
    radioButtons["mean2_percentage"] = ui->radioButton_percentage->isChecked();
    radioButtons["decision1"] = ui->radioButton_decision1->isChecked();
    radioButtons["decision2"] = ui->radioButton_decision2->isChecked();
    radioButtons["decision3"] = ui->radioButton_decision3->isChecked();

    emit this->valuesChanged(params, radioButtons);
}

void Settings::setValue(const QHash<QString, double> &params)
{
    ui->textEdit_R1->setText(QString::number(params["R1"]));
    ui->textEdit_R2->setText(QString::number(params["R2"]));
    ui->textEdit_loss->setText(QString::number(params["loss"]));
    ui->textEdit_prob_s1->setText(QString::number(params["prob_s1"]));
    ui->textEdit_prob_s2->setText(QString::number(params["prob_s2"]));
    ui->textEdit_min_observation->setText(QString::number(params["min_observation"]));
    ui->textEdit_std_dev->setText(QString::number(params["std_dev_range"]));
    ui->textEdit_mean2_zscore->setText(QString::number(params["mean2_zscore"]));
    ui->textEdit_mean2_percentage->setText(QString::number(params["mean2_percentage"]));
}

void Settings::on_pushButton_clicked()
{
    this->textEdit();
    this->close();
}

void Settings::on_radioButton_std_dev_clicked()
{
    ui->label_mean2_percentage->hide();
    ui->textEdit_mean2_percentage->hide();
    ui->label_mean2_zscore->show();
    ui->textEdit_mean2_zscore->show();
}

void Settings::on_radioButton_percentage_clicked()
{
    ui->label_mean2_percentage->show();
    ui->textEdit_mean2_percentage->show();
    ui->label_mean2_zscore->hide();
    ui->textEdit_mean2_zscore->hide();
}
