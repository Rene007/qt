#include <QVector>
#include <QHash>
#include <QString>
#include <QColor>
#include "configurations.h"

//For PDF and Observation Plots
PDFConstants pdf_constants;

//For Impact Plot
ImpactConstants impact_constants;

//For colors
Colors colors;

//For Attack/NoAttaack decisions
Decisions decisions;

//For Probabilities
Probabilities prob;

//For AutoMode
static AutoMode automode;

//For Output File
QVector<QHash<QString, double>> outer_data;
QHash<QString, double> inner_data;
QHash<QString, double> params;
QHash<QString, int> packets_count;

//For New Observation
static int old_data_size = 0;
static double old_mean = 0;
static double old_standard_deviation = 0;
static double old_variance = 0;
static double old_threshold = 0;
static double old_thresholdPrime = 0;

//For ROC Plot
static QVector<double> FPR;
static QVector<double> TPR;

//For swapping between decision window and ROC window
bool is_decision_window = true;

//For Auto Mode Button
bool is_automode = true;

//For PCAP Loaded File
bool is_pcap_mode = true;
