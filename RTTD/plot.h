#ifndef PLOT_H
#define PLOT_H

#include "qcustomplot.h"
#include "coreutils.h"
#include "configurations.h"

namespace Ui  {
    class Plot;
}

/**
 * @brief The Plot class holds all functions used
 * for plotting (PDF, Impact, ROC, etc.)
 * @author Rene Midouin
 * @version 1.0
 */
class Plot
{
    public:
        Plot(QCustomPlot *ui_plot);
        /**
         * @brief setThresholdLabel sets the optimal_threshold label on a plot
         * @param threshold The ui label
         */
        void setThresholdLabel(QLabel *threshold);

        /**
         * @brief setConstants sets the Impact constants for the loss functions in the Impact graph. See @ref Impact.
         * @param constants Impact Constants. See @ref ImpactConstants
         */
        void setConstants(const ImpactConstants &constants);

        /**
         * @brief setConstants sets the PDF constants for a normal distribution graph
         * @param constants PDF Constants. See @ref PDFConstants
         */
        void setConstants(const PDFConstants &constants);

        /**
         * @brief Impact plots the two loss functions
         * @param type Negative or Positive loss function, LINE_INTERCEPT to plot the line intercept between the two loss functions
         * @param plot_index Graph number (graph 1, graph2, etc ...) of graphs plotted on the same axis
         * @param mu The mean of the loss functions
         * @param color The color of the loss function to be plotted
         */
        void Impact(const std::string &type, const int &plot_index, const double &mu, const QColor &color);

        /**
         * @brief PDF plots a standard normal distribution curve
         * @param plot_index Graph number (graph 1, graph2, etc ...) of graphs plotted on the same axis
         * @param mu The mean of the standard normal curve
         * @param sigma The standard deviation of the standard normal curve
         * @param color The color of the standard normal curve to be plotted
         * @param type LINE_INTERCEPT if the line intercept between two normal curves is needed
         */
        void PDF(const int &plot_index, const double &mu, const double &sigma, const QColor &color, const std::string &type = "");

        /**
         * @brief Observation plots a standard normal distribution curve for each new observation
         * @param plot_index Graph number (graph 1, graph2, etc ...) of graphs plotted on the same axis
         * @param mu The mean of the standard normal curve
         * @param sigma The standard deviation of the standard normal curve
         * @param color The color of the standard normal curve to be plotted
         * @param type LINE_INTERCEPT if the line intercept between two normal curves is needed
         */
        void Observation(const int &plot_index, const double &mu, const double &sigma, const QColor &color, const std::string &type = "");

        /**
         * @brief ROC plots the Receiver Operator Characteristics graph which is a useful
         * tool to select classification models based on their performance with respect to
         * the False Positive and True Positive rates.
         * @param plot_index Graph number (graph 1, graph2, etc ...) of graphs plotted on the same axis
         * @param FPR False Positive Rate. See @ref CoreUtils::FPR
         * @param TPR True Positive Rate. See @ref CoreUtils::TPR
         * @param type RANDOM_GUESSING if we need the diagonal of the ROC
         */
        void ROC(const int &plot_index, const QVector<double> &FPR, const QVector<double> &TPR, const std::string &type = "");

        /**
         * @brief setTitle lets us set the title of a plot
         * @param name Title name of the plot
         * @param size Font size of the title
         */
        void setTitle(const QString &name, const int &size = 15);

        /**
         * @brief addData lets us add a single data point to an existing ROC plot. See @ref ROC.
         * @param plot_index Graph number (graph1, graph2, etc ...) of graphs plotted on the same axis
         * @param FPR False Positive Rate value
         * @param TPR True Positive Rate value
         */
        void addData(const int &plot_index, const double &FPR, const double &TPR);

        /**
         * @brief addPointLabel lets us add the point label to an existing ROC ploy. See @ref ROC.
         * @param plot_index Graph number (graph1, graph2, etc ...) of graphs plotted on the same axis
         * @param x_val X-value of the point to be labeled
         * @param y_val Y-value of the point to be labeled
         */
        void addPointLabel(const double &x_val, const double &y_val);

    private:
        QCustomPlot *plot;
        PDFConstants pdf_constants;
        ImpactConstants impact_constants;
        QLabel *threshold_label = nullptr;
        QCPItemText *phaseTracerText = nullptr;
        QCPCurve *curve = nullptr;
        QCPGraph *graph =  nullptr;
        QCPItemTracer *groupTracer = nullptr;
        int isTitleSet;
};

#endif // PLOT_H
