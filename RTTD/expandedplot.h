#ifndef EXPANDEDPLOT_H
#define EXPANDEDPLOT_H

#include <QWidget>
#include "plot.h"
#include "configurations.h"

namespace Ui {
class ExpandedPlot;
}

class ExpandedPlot : public QWidget
{
    Q_OBJECT

    public:
        explicit ExpandedPlot(QWidget *parent = 0);
        ~ExpandedPlot();

    public slots:
        void setConstants(const PDFConstants &constants);

        void PlotObservation(const int &plot_index, const double &mu, const double &sigma,
                             const QColor &color, const std::string &type = "");

    private:
        Ui::ExpandedPlot *ui;
        QSharedPointer<Plot> plot;
};

#endif // EXPANDEDPLOT_H
