var searchData=
[
  ['pdf',['PDF',['../class_plot.html#a241737ab45aa600470b35af04718e9e6',1,'Plot']]],
  ['pdfconstants',['PDFConstants',['../struct_p_d_f_constants.html',1,'']]],
  ['percent_5fshift',['PERCENT_SHIFT',['../struct_p_d_f_constants.html#a0f76715ea925a8b95c9afb627c5c3d0b',1,'PDFConstants']]],
  ['plot',['Plot',['../class_plot.html',1,'']]],
  ['prob_5fs1',['PROB_S1',['../struct_probabilities.html#aa22b25c2502433a3faa045a9b0f72eb1',1,'Probabilities']]],
  ['prob_5fs2',['PROB_S2',['../struct_probabilities.html#a63600e041ab9e18e09eb983c12824639',1,'Probabilities']]],
  ['probabilities',['Probabilities',['../struct_probabilities.html',1,'']]],
  ['purple',['PURPLE',['../struct_colors.html#a24535043969b87cd63eaddf19c1eb08a',1,'Colors']]]
];
