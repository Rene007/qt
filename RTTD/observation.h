#ifndef OBSERVATION_H
#define OBSERVATION_H
#include <QVector>
#include <QtMath>
#include "plot.h"

/**
 * @brief The Observation class is used to manipulate
 * a new observation based on previous observations.
 * @author Rene Midouin
 * @version 1.0
 */
class Observation
{    
    public:
        /**
         * @brief Observation creates a new instance of the class
         * @param data Vector of doubles holding previous list of data
         * @param new_observation New data that will be added to list of previous data
         */
        Observation(const QVector<double>& data, const double &new_observation);

        /**
         * @brief Observation creates a new instance of the class
         * @param new_observation New data that will be added to list of previous data
         * @param old_mean Mean of previous list of data
         * @param old_variance Variance of previous list of data
         * @param data_size Previous data size
         */
        Observation(const double &new_observation, const double &old_mean, const double &old_variance, const int &data_size);

        /**
         * @return List of previous data
         */
        QVector<double> getOldData();

        /**
         * @return List of current data
         */
        QVector<double> getNewData();

        /**
         * @return Mean of current data
         */
        double Mean();

        /**
         * @return Standard deviation of current data
         */
        double StandardDeviation();

        /**
         * @return Variance of current of data
         */
        double Variance();
    private:
        QVector<double> old_data, new_data;
        int data_size;
        double new_observation, old_mean, old_variance, old_standard_deviation, new_mean, new_variance, new_standard_deviation;
};

#endif // OBSERVATION_H
