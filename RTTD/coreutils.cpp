#include "coreutils.h"
#include <QDebug>
#include <QWindow>
#include <Qt>

CoreUtils::CoreUtils()
{

}

double CoreUtils::normPDF(double x, double mu, double sigma)
{
    double variance = sigma * sigma;
    double z_score = (x - mu)/sigma;
    double result = (1 / qPow(2 * M_PI * variance, 0.5)) * qExp(-1 * (qPow(z_score, 2) / 2));
    return result;
}

double CoreUtils::normCDF(double x, double mu, double sigma)
{
    //Constants
    double p = 0.2316419;
    double b1 = 0.319381530;
    double b2 = -0.35656378;
    double b3 = 1.7814779;
    double b4 = -1.821256;
    double b5 = 1.3302744;

    double z = (x - mu)/sigma;
    double val = z;

    if (z < 0)
        val = -1*z;

    double t = 1 / (1 + p*val);

    double Q = (1/(qPow(2*M_PI, 0.5))) * qExp(-1*qPow(val,2)/2) * (b1*t + b2*qPow(t,2) + b3*qPow(t,3) + b4*qPow(t,4) + b5*qPow(t,5));

    if (z < 0)
        return Q;
    else
        return 1 - Q;
}

double CoreUtils::FPR(const double &FP, const double &TN)
{
    return FP/(FP + TN);
}

double CoreUtils::TPR(const double &TP, const double &FN)
{
    return TP/(FN + TP);

}

double CoreUtils::impactIntercept(const double &threshold, const ImpactConstants &impact_constants)
{
    double R = impact_constants.R1/impact_constants.R2;
    double Z = (R-1)/2;
    double x1 = Z + qPow(Z*Z + 1,0.5);
    double x2 = Z - qPow(Z*Z + 1,0.5);
    x1 = (qLn(x1)/impact_constants.LOSS) + threshold + 0.01;
    x2 = (qLn(x2)/impact_constants.LOSS) + threshold + 0.01;
    return (x2 > x1) ? x2 : x1;
}

bool CoreUtils::decisionFunction(const double &p1, const double &p2, const double &observation, const double &optimal_threshold, const PDFConstants &pdf_constants)
{
    if (pdf_constants.isDecision1) {
        return p2 > p1;
    } else if (pdf_constants.isDecision2) {
        return (p2 > p1) && (observation > optimal_threshold);
    } else if (pdf_constants.isDecision3) {
        return (p2 > p1) || (observation > optimal_threshold);
    } else if (p1 > p2) {
        return false;
    } else {
        return true;
    }
}

bool CoreUtils::isValidObservation(const double &observation, const double &mu, const double &sigma, const PDFConstants &pdf_constants)
{
    double valid_left_range_1 = mu - pdf_constants.STD_DEV_RANGE * sigma;
    double valid_left_range_2 = (pdf_constants.MIN_OBSERVATION/100) * mu;
    double valid_left_range = (valid_left_range_1 > valid_left_range_2) ? valid_left_range_1 : valid_left_range_2;

    /**
     * No need for valid_right_range now. DO NOT LIMIT RIGHT RANGE FOR NOW.
     * If right value is too large, it is an attack.
     */
    //double valid_right_range = mu + pdf_constants.STD_DEV_RANGE * sigma;

    if (observation < valid_left_range)
        return false;
    else
        return true;
}

double CoreUtils::mean(const QVector<double> &data)
{
    double result = 0;
    int data_size = data.size();
    for (int i = 0; i < data_size; ++i) {
        result += data[i];
    }
    result /= data_size;
    return result;
}

double CoreUtils::std_dev(const QVector<double> &data)
{
    return qPow(variance(data), 0.5);
}

double CoreUtils::variance(const QVector<double> &data)
{
    double result = 0;
    int data_size = data.size();
    double data_mean = mean(data);
    for (int i = 0; i < data_size; ++i) {
        result += qPow(data[i] - data_mean, 2);
    }
    result /= (data_size - 1);
    return result;
}

double CoreUtils::shifted_mean(const double &mu, const double &sigma, const PDFConstants &pdf_constants)
{
    if (pdf_constants.isRadio_Std_Dev) {
      double muShifted1 = mu + pdf_constants.SHIFTED_MEAN_Z_SCORE * sigma;
      return muShifted1;
    }
    if (pdf_constants.isRadio_Percentage) {
      double muShifted2 = mu * (1 + pdf_constants.PERCENT_SHIFT);
      return muShifted2;
    }
}
