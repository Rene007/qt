#ifndef RTTD_H
#define RTTD_H

#include <QMainWindow>
#include <QVector>
#include <QTextEdit>
#include <QHash>
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>
#include <chrono>
#include "qcustomplot.h"
#include "settings.h"
#include "configurations.h"
#ifdef __APPLE__
#include <sys/types.h>
#endif

namespace Ui
{
    class RTTD;
}

class RTTD : public QMainWindow
{
    Q_OBJECT

    public:
        explicit RTTD(QWidget *parent = 0);
        ~RTTD();

    protected:
        void Graph();
        void RandomResults();

    private slots:
        void on_pushObservation_clicked();
        void on_pushGraph_clicked();
        void RandomData(double min = 0.0, double max = 0.0, int number_of_points = 0);
        void NewObservation(double new_observation);
        void on_exportData_clicked();
        void on_settings_clicked();
        void on_extend_observation_plot_clicked();
        void on_simulation_clicked();

        void on_switch_decision_roc_clicked();

        void on_auto_mode_clicked();

        void on_auto_mode_settings_clicked();

        void on_actionLoadFile_triggered();

        void on_actionRun_triggered();

signals:
        void valuesChanged(const int &plot_index, const double &mu, const double &sigma,
                           const QColor &color, const std::string &type="");
        void sendMinMax(const double &min, const double &max, const double &multiplier);
        void sendConstants(const PDFConstants &constants);
        void sendSimulationValues(const AutoMode &automode_constants);
        void sendPacketsCount(const QHash<QString, int> &packets_count);

    public slots:
        void setValue(const QHash<QString, double> &params, const QHash<QString, bool> &radioButtons);
        void setAutoModeValues(const QHash<QString, double> &params);
        void observation_generator();
        void pcap_observation_generator();
        static void packetHandler(u_char *userData, const struct pcap_pkthdr* pkthdr, const u_char* packet);


    private:
        Ui::RTTD *ui;
        QTimer *automode_timer;
        QTimer *pcap_mode_timer;
        QAction *start_pcap;
        QAction *stop_pcap;
};

#endif // RTTD_H
