#ifndef SIMULATION_H
#define SIMULATION_H

#include <QWidget>
#include "configurations.h"
#include <qapplication.h>
#include <QLabel>

namespace Ui {
class Simulation;
}

class Simulation : public QWidget
{
    Q_OBJECT

public:
    explicit Simulation(QWidget *parent = 0);
    ~Simulation();

public slots:
    void simulate(const AutoMode &automode_constants);
    void updatePacketsCount(const QHash<QString, int> &packets_count);

private slots:
    void movePacket();
    void changePacketColor(QLabel *packet_object, QString color);
    void changePacketColorByType(QLabel *packet_object, int type);

private:
    Ui::Simulation *ui;
    QTimer *timer;
};

#endif // SIMULATION_H
