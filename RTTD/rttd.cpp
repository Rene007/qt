#include "rttd.h"
#include "ui_rttd.h"
#include "observation.h"
#include "file.h"
#include "settings.h"
#include "simulation.h"
#include "automode_settings.h"

// LIBPCAP LIBRARY
#include "pcap/pcap.h"
#include "net/ethernet.h"
#include "netinet/ip.h"
#include "netinet/in.h"
#include "netinet/tcp.h"
#include "arpa/inet.h"

#include "expandedplot.h"
#include "constants.cpp"
#include "plot.h"
#include "coreutils.h"

//Change "file_name" accordingly (make sure to include full path)
QString FILE_NAME  = "/Users/Rene/Desktop/out.csv";
std::string my_pcap_file = "/Users/Rene/Desktop/test.pcap";

u_int sequence_a, sequence_b, ack_a, ack_b, delta_t, delta_t1, delta_t2, delta_a, delta_b, sequence_count;
QVector<double> pcap_data;
int pcap_iterator;

ExpandedPlot *extended_observation_plot = nullptr;
Settings *settings = nullptr;
Simulation *simulation = nullptr;
AutoMode_Settings *automode_settings = nullptr;

QScopedPointer<Plot> plot1;
QScopedPointer<Plot> plot2;
QScopedPointer<Plot> plot3;
QScopedPointer<Plot> plot4;

RTTD::RTTD(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::RTTD)
{
    sequence_a = -1;
    sequence_b = -1;
    ack_a = -1;
    ack_b = -1;
    delta_t = 0;
    delta_a = 0;
    delta_b = 0;
    sequence_count = 0;

    pcap_iterator = 0;

    ui->setupUi(this);
    automode_timer = new QTimer(this);
    pcap_mode_timer = new QTimer(this);
    simulation = new Simulation();

    packets_count["good"] = 0;
    packets_count["bad"] = 0;
    packets_count["out_of_range"] = 0;

    QObject::connect(automode_timer, SIGNAL(timeout()), this, SLOT(observation_generator()));
    QObject::connect(pcap_mode_timer, SIGNAL(timeout()), this, SLOT(pcap_observation_generator()));
    QObject::connect(this, &RTTD::sendSimulationValues, simulation, &Simulation::simulate);
    QObject::connect(this, &RTTD::sendPacketsCount, simulation, &Simulation::updatePacketsCount);

    RTTD::RandomData(40,50);
}
RTTD::~RTTD()
{
    delete ui;
}

void RTTD::Graph()
{
    //Clear vectors that hold output data
    outer_data.clear();
    outer_data.clear();
    params.clear();
    packets_count.clear();

    //Clear vectors that hold ROC Graph data
    FPR.clear();
    TPR.clear();

    int num_points = 100;
    QString min = ui->textMin->toPlainText();
    QString max = ui->textMax->toPlainText();
    QString number_of_points = ui->textNumPoints->toPlainText();
    if (number_of_points != "")
        num_points = number_of_points.toInt();

    RandomData(min.toDouble(), max.toDouble(), num_points);
    QString result;
    result.sprintf("%d", num_points);
    ui->labelNumPoints->setText(result);

    emit this->sendMinMax(min.toDouble(), max.toDouble(), automode.MULTIPLIER);
    emit this->sendPacketsCount(packets_count);
    automode.MIN = min.toDouble();
    automode.MAX = max.toDouble();
}

void RTTD::on_pushGraph_clicked()
{
    Graph();
}

void RTTD::RandomData(double min, double max, int number_of_points)
{
    // Reset Graphs
    ui->pdfPlot->clearGraphs();
    ui->observationPlot->clearGraphs();
    ui->impactPlot->clearGraphs();
    ui->rocPlot->clearGraphs();

    if (!automode_settings) {
        automode_settings = new AutoMode_Settings();
        QObject::connect(this, &RTTD::sendMinMax, automode_settings, &AutoMode_Settings::setValue);
        QObject::connect(automode_settings, &AutoMode_Settings::valuesChanged, this, &RTTD::setAutoModeValues);
        emit this->sendMinMax(min, max, automode.MULTIPLIER);
    }

    QString previousMean, currentMean, previousVariance, currentVariance,previousThreshold, currentThreshold, RTTD_Threshold;
    double RANGE = max - min;

    if (!number_of_points)
        number_of_points = 100;

    if (number_of_points == 1)
        number_of_points = 2;

    QVector<double> previousData(number_of_points);

    for (int i = 0; i < previousData.size(); ++i) {
        previousData[i] = min + RANGE*(qrand()/(double)RAND_MAX);
    }

    double pstdev = CoreUtils::std_dev(previousData);
    double cstdev = pstdev;

    double pvariance = CoreUtils::variance(previousData);
    double cvariance = pvariance;

    double pmu = CoreUtils::mean(previousData);
    double pmuShifted = CoreUtils::shifted_mean(pmu, pstdev, pdf_constants);

    double cmu = pmu;
    double cmuShifted = pmuShifted;

    double pthreshold = (pmu + pmuShifted) / 2;
    double cthreshold = (pmu + pmuShifted) / 2;

    old_data_size= previousData.size();
    old_mean = cmu;
    old_standard_deviation = cstdev;
    old_variance = cvariance;
    old_threshold = cthreshold;
    old_thresholdPrime = cthreshold;

    /**
     * Save the following parameters: min, max, number of observations
     * Save previous mean (A.K.A mean1), shifted
     * mean (A.K.A mean2), previous and current standard
     * deviation (A.K.A var1 and var2), previous threshold (A.K.A thr1),
     * loss value obtained from loss functions (A.K.A loss)
     * */

    params["min"] = min;
    params["max"] = max;
    params["num_of_observations"] = static_cast<double>(number_of_points);
    inner_data["observation_number"] = static_cast<double>(number_of_points);
    inner_data["mean1"] = cmu;
    inner_data["var1"] = cvariance;
    inner_data["thr1"] = cthreshold;
    inner_data["loss"] = CoreUtils::impactIntercept(cthreshold, impact_constants);
    inner_data["mean2"] = cmuShifted;
    inner_data["var2"] = cvariance;
    inner_data["p_s1"] = prob.PROB_S1;
    inner_data["p_s2"] = prob.PROB_S2;
    outer_data.push_back(inner_data);

    previousMean.sprintf("%.3f", pmu);
    previousVariance.sprintf("%.3f",pvariance);
    previousThreshold.sprintf("%.3f",pthreshold);

    ui->labelPreviousMean->setText(previousMean);
    ui->labelPreviousVariance->setText(previousVariance);
    ui->labelPreviousThreshold->setText(previousThreshold);

    currentMean.sprintf("%.3f", cmu);
    currentVariance.sprintf("%.3f",cvariance);
    currentThreshold.sprintf("%.3f",cthreshold);

    ui->labelCurrentMean->setText(currentMean);
    ui->labelCurrentVariance->setText(currentVariance);
    ui->labelCurrentThreshold->setText(currentThreshold);


    if (!plot1) plot1.reset(new Plot(ui->pdfPlot));
    plot1->setThresholdLabel(ui->RTTD_Threshold);
    plot1->setConstants(pdf_constants);
    plot1->PDF(0, pmu, pstdev, colors.BLUE);
    plot1->PDF(1, pmuShifted, pstdev, colors.ORANGE);
    plot1->PDF(2, pthreshold, pstdev, colors.BLACK, "LINE_INTERCEPT");

    if (!plot2) plot2.reset(new Plot(ui->observationPlot));
    plot2->setThresholdLabel(ui->Observation_Threshold);
    plot2->setConstants(pdf_constants);
    plot2->Observation(0, pmu, pstdev, colors.BLUE);
    plot2->Observation(1, pmuShifted, pstdev, colors.ORANGE);
    plot2->Observation(2, pthreshold, pstdev, colors.BLACK, "LINE_INTERCEPT");

    if (!plot3) plot3.reset(new Plot(ui->impactPlot));
    plot3->setThresholdLabel(ui->labelThreshold);
    plot3->setConstants(impact_constants);
    plot3->Impact("POSITIVE", 0, cthreshold, colors.BLUE);
    plot3->Impact("NEGATIVE", 1, cthreshold, colors.ORANGE);
    plot3->Impact("LINE_INTERCEPT", 2, cthreshold, colors.BLACK);

    if (!plot4) plot4.reset(new Plot(ui->rocPlot));
    plot4->setThresholdLabel(ui->ROC_Threshold);
    plot4->ROC(0, FPR, TPR, "RANDOM_GUESSING");

    // Re-initialize variables
    pdf_constants.LEFTX= RAND_MAX;
    pdf_constants.RIGHTX = -1* RAND_MAX;
    pdf_constants.OLEFTX = RAND_MAX;
    pdf_constants.ORIGHTX = -1* RAND_MAX;
    ui->decision->setText(decisions.NOATTACK);
    extended_observation_plot = nullptr;
    simulation = nullptr;
    ui->decision->setStyleSheet("QLabel {color: green}");
    ui->s1_given_x->setText("Prob(s1|x) = ...");
    ui->s2_given_x->setText("Prob(s2|x) = ...");
    ui->x_given_s1->setText("Prob(x|s1) = ...");
    ui->x_given_s2->setText("Prob(x|s2) = ...");
    ui->prob_x->setText("Prob(x) = ...");
}

void RTTD::NewObservation(double observation)
{
    QString number_of_observations, previousMean, currentMean,
            previousVariance, currentVariance, previousThreshold,
            currentThreshold, RTTD_Threshold, observationThreshold;

    if (!extended_observation_plot) {
        extended_observation_plot = new ExpandedPlot();
        QObject::connect(this, &RTTD::valuesChanged, extended_observation_plot, &ExpandedPlot::PlotObservation);
        QObject::connect(this, &RTTD::sendConstants, extended_observation_plot, &ExpandedPlot::setConstants);
    }

    Observation* new_observation = new Observation(observation, old_mean, old_variance, old_data_size++);

    /**
     * NEW AND FAST METHOD OF COMPUTING NEW MEAN AND NEW STANDARD DEVIATION
     * FROM PREVIOUS MEAN AND PREVIOUS STANDARD DEVIATION
     * */

    //previous
    double pmu = old_mean;
    double pstdev = old_standard_deviation;
    double pvariance = old_variance;
    double pmuShifted = CoreUtils::shifted_mean(pmu, pstdev, pdf_constants);
    double pthresholdPrime = (pmu + pmuShifted) / 2;
    double pthreshold = old_threshold;

    //current
    double cmu = new_observation->Mean();
    double cstdev = new_observation->StandardDeviation();
    double cvariance = new_observation->Variance();
    double cmuShifted = CoreUtils::shifted_mean(cmu, cstdev, pdf_constants);
    double cthresholdPrime = (cmu + cmuShifted) / 2;
    double cthreshold = CoreUtils::impactIntercept(old_thresholdPrime, impact_constants);

    number_of_observations.sprintf("%d", old_data_size);
    ui->labelNumPoints->setText(number_of_observations);

    previousMean.sprintf("%.3f", pmu);
    previousVariance.sprintf("%.3f",pvariance);
    previousThreshold.sprintf("%.3f",pthresholdPrime);

    ui->labelPreviousMean->setText(previousMean);
    ui->labelPreviousVariance->setText(previousVariance);
    ui->labelPreviousThreshold->setText(previousThreshold);

    currentMean.sprintf("%.3f", cmu);
    currentVariance.sprintf("%.3f",cvariance);
    currentThreshold.sprintf("%.3f",cthresholdPrime);

    ui->labelCurrentMean->setText(currentMean);
    ui->labelCurrentVariance->setText(currentVariance);
    ui->labelCurrentThreshold->setText(currentThreshold);

    if(!plot1) plot1.reset(new Plot(ui->pdfPlot));
    plot1->setThresholdLabel(ui->RTTD_Threshold);
    plot1->setConstants(pdf_constants);
    plot1->PDF(0, pmu, pstdev, colors.BLUE);
    plot1->PDF(1, pmuShifted, pstdev, colors.ORANGE);
    plot1->PDF(2, pthresholdPrime, pstdev, colors.BLACK, "LINE_INTERCEPT");
    plot1->PDF(3, pthreshold, pstdev, colors.PURPLE, "LINE_INTERCEPT");

    if (!plot2) plot2.reset(new Plot(ui->observationPlot));
    plot2->setThresholdLabel(ui->Observation_Threshold);
    plot2->setConstants(pdf_constants);
    plot2->Observation(0, cmu, cstdev, colors.BLUE);
    plot2->Observation(1, cmuShifted, cstdev, colors.ORANGE);
    plot2->Observation(2, cthresholdPrime, cstdev, colors.BLACK, "LINE_INTERCEPT");
    plot2->Observation(3, cthreshold, cstdev, colors.PURPLE, "LINE_INTERCEPT");

    emit this->sendConstants(pdf_constants);
    emit this->valuesChanged(0, cmu, cstdev, colors.BLUE);
    emit this->valuesChanged(1, cmuShifted, cstdev, colors.ORANGE);
    emit this->valuesChanged(2, cthresholdPrime, cstdev, colors.BLACK, "LINE_INTERCEPT");
    emit this->valuesChanged(3, cthreshold, cstdev, colors.PURPLE, "LINE_INTERCEPT");

    if (!plot3) plot3.reset(new Plot(ui->impactPlot));
    plot3->setThresholdLabel(ui->labelThreshold);
    plot3->setConstants(impact_constants);
    plot3->Impact("POSITIVE", 0, cthresholdPrime, colors.BLUE);
    plot3->Impact("NEGATIVE", 1, cthresholdPrime, colors.ORANGE);
    plot3->Impact("LINE_INTERCEPT", 2, cthresholdPrime, colors.BLACK);

    if (!plot4) plot4.reset(new Plot(ui->rocPlot));
    double FP, TN, TP, FN, FPR_single, TPR_single;
    if (cthreshold >= cthresholdPrime) {
      FP = CoreUtils::normCDF(cthresholdPrime, cmuShifted, cstdev) + CoreUtils::normCDF(cthreshold, cmu, cstdev) - CoreUtils::normCDF(cthresholdPrime, cmu, cstdev);
      TN = CoreUtils::normCDF(cthresholdPrime, cmu, cstdev) - CoreUtils::normCDF(cthresholdPrime, cmuShifted, cstdev);
      TP = TN;
      FN = 1 - CoreUtils::normCDF(cthreshold, cmu, cstdev);
    } else {
      FP = CoreUtils::normCDF(cthreshold, cmuShifted, cstdev);
      TN = CoreUtils::normCDF(cthresholdPrime, cmu, cstdev) - CoreUtils::normCDF(cthresholdPrime, cmuShifted, cstdev);
      TP = TN;
      FN = 1 - CoreUtils::normCDF(cthresholdPrime, cmu, cstdev) \
              + CoreUtils::normCDF(cthresholdPrime, cmuShifted, cstdev) \
              - CoreUtils::normCDF(cthresholdPrime, cmuShifted, cstdev);
    }
    FPR_single = CoreUtils::FPR(FP, TN);
    TPR_single = CoreUtils::TPR(TP, FN);
    FPR.append(FPR_single);
    TPR.append(TPR_single);
    plot4->ROC(0, FPR, TPR);
    plot4->ROC(1, FPR, TPR, "RANDOM_GUESSING");
    plot4->addPointLabel(FPR_single, TPR_single);
    plot4->addData(2,FPR_single, TPR_single);

    double prob_x_given_s1 = CoreUtils::normPDF(observation, cmu, cstdev);
    double prob_x_given_s2 = CoreUtils::normPDF(observation, cmuShifted, cstdev);
    double prob_x = prob_x_given_s1 * prob.PROB_S1 + prob_x_given_s2 * prob.PROB_S2;
    double prob_s1_given_x = (prob_x_given_s1 * prob.PROB_S1) / prob_x;
    double prob_s2_given_x = (prob_x_given_s2 * prob.PROB_S2) / prob_x;

    QString s1_given_x, s2_given_x, x_given_s1, x_given_s2, label_prob_x;
    s1_given_x.sprintf("P(s1|x) = %f", prob_s1_given_x);
    s2_given_x.sprintf("P(s2|x) = %f", prob_s2_given_x);
    x_given_s1.sprintf("P(x|s1) = %f", prob_x_given_s1);
    x_given_s2.sprintf("P(x|s2) = %f", prob_x_given_s2);
    label_prob_x.sprintf("P(x) = %f", prob_x);

    ui->s1_given_x->setText(s1_given_x);
    ui->s2_given_x->setText(s2_given_x);
    ui->x_given_s1->setText(x_given_s1);
    ui->x_given_s2->setText(x_given_s2);
    ui->prob_x->setText(label_prob_x);

    // Re-initialize variables
    pdf_constants.LEFTX = RAND_MAX;
    pdf_constants.RIGHTX = -1* RAND_MAX;
    pdf_constants.OLEFTX = RAND_MAX;
    pdf_constants.ORIGHTX = -1* RAND_MAX;

    // Set old data to new data for next observation
    // if data is within valid range and p(s1|x) > p(s2|x) --> NO ATTACK was detected at this point

    // t1 is used to calculate how long it takes to make a decision
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

    bool isValidData = CoreUtils::isValidObservation(observation, cmu, cstdev, pdf_constants);
    if (!isValidData) {
        ui->decision->setStyleSheet("QLabel {color: yellow}");
        ui->decision->setText(decisions.WARNING);

        old_mean = pmu;
        old_standard_deviation = pstdev;
        old_variance = pvariance;
        old_threshold = pthreshold;
        old_data_size -= 1;
        packets_count["out_of_range"]++;
        automode.TYPE = 2;
        emit this->sendPacketsCount(packets_count);
    } else {
        if (CoreUtils::decisionFunction(prob_s1_given_x, prob_s2_given_x, observation, cthreshold, pdf_constants)) {
            ui->decision->setStyleSheet("QLabel {color: red}");
            ui->decision->setText(decisions.ATTACK);
            packets_count["bad"]++;
            automode.TYPE = 1;
            emit this->sendPacketsCount(packets_count);

            old_mean = pmu;
            old_standard_deviation = pstdev;
            old_variance = pvariance;
            old_threshold = pthreshold;
            old_data_size -= 1;
        } else {
            ui->decision->setStyleSheet("QLabel {color: green}");
            ui->decision->setText(decisions.NOATTACK);
            packets_count["good"]++;
            automode.TYPE = 0;
            emit this->sendPacketsCount(packets_count);

            inner_data["observation_number"] = static_cast<double>(old_data_size);
            inner_data["reading"] = observation;
            inner_data["mean1"] = cmu;
            inner_data["var1"] = cvariance;
            inner_data["thr1"] = cthresholdPrime;
            inner_data["loss"] = CoreUtils::impactIntercept(cthresholdPrime, impact_constants);
            inner_data["mean2"] = cmuShifted;
            inner_data["var2"] = cvariance;
            inner_data["p_s1"] = prob.PROB_S1;
            inner_data["p_s1_given_x"] = prob_s1_given_x;
            inner_data["p_x_given_s1"] = prob_x_given_s1;
            inner_data["p_s2"] = prob.PROB_S2;
            inner_data["p_s2_given_x"] = prob_s2_given_x;
            inner_data["p_x_given_s2"] = prob_x_given_s2;
            outer_data.push_back(inner_data);

            old_mean = cmu;
            old_standard_deviation = cstdev;
            old_variance = cvariance;
            old_threshold = cthreshold;
            old_thresholdPrime = cthresholdPrime;
        }
    }

    // t2 is used to calculate how long it takes to make a decision
    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
    ui->decision_execution_time->setText(QString::number(duration));
}

void RTTD::on_pushObservation_clicked()
{
    int new_observation = 0;
    QString observation = ui->textObservation->toPlainText();
    if (observation != "")
        new_observation = observation.toDouble();

    NewObservation(new_observation);
}

void RTTD::on_exportData_clicked()
{
    File *new_file = new File(FILE_NAME);
    new_file->SaveReadingValues(params, outer_data);
    QMessageBox::information(this, "Export File", "Your file has been successfully exported to "+FILE_NAME);
}

void RTTD::setValue(const QHash<QString, double> &params, const QHash<QString, bool> &radioButtons)
{
    impact_constants.R1 = params["R1"];
    impact_constants.R2 = params["R2"];
    impact_constants.LOSS = params["loss"];
    prob.PROB_S1= params["prob_s1"];
    prob.PROB_S2 = params["prob_s2"];
    pdf_constants.MIN_OBSERVATION = params["min_observation"];
    pdf_constants.STD_DEV_RANGE = params["std_dev_range"];
    pdf_constants.SHIFTED_MEAN_Z_SCORE = params["mean2_zscore"];
    pdf_constants.PERCENT_SHIFT = params["mean2_percentage"] / 100;
    pdf_constants.isRadio_Std_Dev = radioButtons["mean2_zscore"];
    pdf_constants.isRadio_Percentage = radioButtons["mean2_percentage"];
    pdf_constants.isDecision1 = radioButtons["decision1"];
    pdf_constants.isDecision2 = radioButtons["decision2"];
    pdf_constants.isDecision3 = radioButtons["decision3"];

    Graph();
}

void RTTD::setAutoModeValues(const QHash<QString, double> &params)
{
    automode.MIN = params["min_observation"];
    automode.MAX = params["max_observation"];
    automode.IAT = params["IAT"];
    automode.MULTIPLIER = params["multiplier"];
}

void RTTD::observation_generator()
{
    double RANGE = automode.MAX - automode.MIN;
    double observation = automode.MIN + RANGE*(qrand()/(double)RAND_MAX);
    int total_time = (int) ((observation + automode.IAT) * automode.MULTIPLIER);
    automode.observation = observation;
    ui->textObservation->setText(QString::number(observation));
    NewObservation(observation);
    emit this->sendSimulationValues(automode);
    automode_timer->setInterval(total_time);
}

void RTTD::pcap_observation_generator()
{
    if (pcap_iterator < pcap_data.size()) {
        double observation = pcap_data[pcap_iterator];
        int total_time = (int) ((observation + automode.IAT) * automode.MULTIPLIER);
        qInfo() << "PCAP DATA: " << endl;
        qInfo() << observation << endl;
        automode.observation = observation;
        ui->textObservation->setText(QString::number(observation));
        NewObservation(observation);
        emit this->sendSimulationValues(automode);
        pcap_mode_timer->setInterval(total_time);
        pcap_iterator++;
    } else {
        pcap_mode_timer->stop();
    }
}

void RTTD::packetHandler(u_char *userData, const pcap_pkthdr *pkthdr, const u_char *packet)
{
    const struct ether_header* ethernetHeader;
    const struct ip* ipHeader;
    const struct tcphdr* tcpHeader;
    char sourceIp[INET_ADDRSTRLEN];
    char destIp[INET_ADDRSTRLEN];
    u_int sourcePort, destPort, sequence_number, ack_number;
    u_int timestamp;
    u_char *data;
    int dataLength = 0;
    char * dataStr = "";

    ethernetHeader = (struct ether_header*)packet;

    if (ntohs(ethernetHeader->ether_type) == ETHERTYPE_IP) {
        ipHeader = (struct ip*)(packet + sizeof(struct ether_header));
        inet_ntop(AF_INET, &(ipHeader->ip_src), sourceIp, INET_ADDRSTRLEN);
        inet_ntop(AF_INET, &(ipHeader->ip_dst), destIp, INET_ADDRSTRLEN);

        tcpHeader = (tcphdr*)(packet + sizeof(struct ether_header) + sizeof(struct ip));
        sourcePort = ntohs(tcpHeader->th_sport);
        destPort = ntohs(tcpHeader->th_dport);
        sequence_number = ntohs(tcpHeader->th_seq);
        ack_number = ntohs(tcpHeader->th_ack);
        timestamp = ntohs(pkthdr->ts.tv_usec);
        data = (u_char*)(packet + sizeof(struct ether_header) + sizeof(struct ip) + sizeof(struct tcphdr));
        dataLength = pkthdr->len - (sizeof(struct ether_header) + sizeof(struct ip) + sizeof(struct tcphdr));


        // print the results

        qInfo() << "time: " << timestamp << "->"
                << "packet: " << packet
                << sourceIp << ":" << sourcePort << " -> " << destIp << ":" << destPort
                << "sequence = " << sequence_number
                << "ack = " << ack_number << endl;

        sequence_b = sequence_a;
        sequence_a = sequence_number;

        delta_b = delta_a;
        delta_a = timestamp;

        ack_b = ack_a;
        ack_a = ack_number;


        if (sequence_b == ack_a && ack_a != ack_b && sequence_count != 2) {
            delta_t = (delta_b > delta_a) ? delta_b - delta_a : delta_a - delta_b;
            delta_t2 = delta_t1;
            delta_t1 = delta_t;
            qInfo() << "delta_a is: " << delta_a << "delta_b is: " << delta_b << "delta_t_sub is: " << delta_t1 << endl;
            sequence_count++;
        } else if (sequence_b == ack_a && ack_a != ack_b && sequence_count == 2) {
            //We're safe. Send data: delta_t
            qInfo() <<"We're safe. send delta_t"<< endl;
            qInfo() << "Count " << sequence_count << "delta_t1: "<< delta_t1<<" delta_t2 time: "<<delta_t2 <<"delta_t: " << delta_t << endl;
            pcap_data.push_back(delta_t/1000.0);
            sequence_count = 0;

            qInfo() << "time: " << timestamp << "->"
                    << "packet: " << packet
                    << sourceIp << ":" << sourcePort << " -> " << destIp << ":" << destPort
                    << "sequence = " << sequence_number
                    << "ack = " << ack_number << endl;

            // Calculate and reset delta_t
            delta_t = (delta_b > delta_a) ? delta_b - delta_a : delta_a - delta_b;
            delta_t2 = delta_t1;
            delta_t1 = delta_t;
            qInfo() << "delta_a is: " << delta_a << "delta_b is: " << delta_b << "delta_t_sub is: " << delta_t1 << endl;
            sequence_count++;
        } else {}

        if (sequence_b != ack_a && ack_a == ack_b && sequence_count != 2) {
            qInfo() << "delta_a + is: " << delta_a << "delta_b + is: " << delta_b << "delta_t + is: " << delta_t << endl;
            delta_t1 += (delta_b > delta_a) ? (delta_b - delta_a) : (delta_a - delta_b);
            qInfo() << "delta plus: " << delta_t1 << endl;
        } else if (sequence_b != ack_a && ack_a == ack_b && sequence_count == 2) {
            //We're not safe. Add delta to RTTD.
            qInfo() <<"We ARE NOT safe. add delta_t"<< endl;
            qInfo() << "delta_a + is: " << delta_a << "delta_b + is: " << delta_b << "delta_t + is: " << delta_t << endl;
            delta_t += (delta_b > delta_a) ? (delta_b - delta_a) : (delta_a - delta_b);

            //Send data: delta_t
            qInfo() << "Count " << sequence_count << "delta_t1: "<< delta_t1<<" delta_t2 time: "<<delta_t2 <<"delta_t: " << delta_t << endl;
            pcap_data.push_back(delta_t/1000.0);
            sequence_count = 0;
        } else {}

        qInfo() <<"This is the sequence_count: " << sequence_count << endl;

        if (sequence_count == 2) {
            delta_t = delta_t1 + delta_t2;
        }
    }
}

void RTTD::on_settings_clicked()
{
    if (!settings) {
        settings = new Settings();
        params["R1"] = impact_constants.R1;
        params["R2"] = impact_constants.R2;
        params["loss"] = impact_constants.LOSS;
        params["prob_s1"] = prob.PROB_S1;
        params["prob_s2"] = prob.PROB_S2;
        params["min_observation"] = pdf_constants.MIN_OBSERVATION;
        params["std_dev_range"] = pdf_constants.STD_DEV_RANGE;
        params["mean2_zscore"] = pdf_constants.SHIFTED_MEAN_Z_SCORE;
        params["mean2_percentage"] = pdf_constants.PERCENT_SHIFT * 100;
        settings->setValue(params);
        QObject::connect(settings, &Settings::valuesChanged, this, &RTTD::setValue);
        settings->show();
    } else {
        settings->show();
    }
}

void RTTD::on_extend_observation_plot_clicked()
{
    if (!extended_observation_plot) {
        double mu = old_mean;
        double sigma = old_standard_deviation;
        double muShifted = CoreUtils::shifted_mean(mu, sigma, pdf_constants);
        double pthreshold = old_thresholdPrime;
        extended_observation_plot = new ExpandedPlot();
        QObject::connect(this, &RTTD::valuesChanged, extended_observation_plot, &ExpandedPlot::PlotObservation);
        QObject::connect(this, &RTTD::sendConstants, extended_observation_plot, &ExpandedPlot::setConstants);
        extended_observation_plot->PlotObservation(0, mu, sigma, colors.BLUE);
        extended_observation_plot->PlotObservation(1, muShifted, sigma, colors.ORANGE);
        extended_observation_plot->PlotObservation(2, pthreshold, sigma, colors.BLACK, "LINE_INTERCEPT");
        extended_observation_plot->show();
    } else {
        extended_observation_plot->show();
    }
}

void RTTD::on_simulation_clicked()
{
    if (!simulation) {
        simulation = new Simulation();
        QObject::connect(this, &RTTD::sendPacketsCount, simulation, &Simulation::updatePacketsCount);
        simulation->show();
    } else {
        QObject::connect(this, &RTTD::sendPacketsCount, simulation, &Simulation::updatePacketsCount);
        simulation->show();
    }
}

void RTTD::on_switch_decision_roc_clicked()
{
    if (is_decision_window) {
        ui->rocPlot->hide();
        ui->switch_decision_roc->setText("ROC PLOT");
        is_decision_window = false;
    } else {
        ui->rocPlot->show();
        ui->switch_decision_roc->setText("DECISION");
        is_decision_window = true;
    }
}

void RTTD::on_auto_mode_clicked()
{
    if(is_automode) {
        ui->auto_mode->setText("Stop Auto Mode");
        ui->auto_mode->setAutoFillBackground(true);
        ui->auto_mode->setStyleSheet("color: rgb(255, 0, 0)");
        automode_timer->start();
        is_automode = false;
        automode.STATUS = true;
        emit this->sendSimulationValues(automode);
    } else {
        ui->auto_mode->setText("Start Auto Mode");
        ui->auto_mode->setAutoFillBackground(true);
        ui->auto_mode->setStyleSheet("color: rgb(0, 0, 0)");
        automode_timer->stop();
        is_automode = true;
        automode.STATUS = false;
        emit this->sendSimulationValues(automode);
    }
}

void RTTD::on_auto_mode_settings_clicked()
{
    if (!automode_settings) {
        automode_settings = new AutoMode_Settings();
        QObject::connect(automode_settings, &AutoMode_Settings::valuesChanged, this, &RTTD::setAutoModeValues);
        automode_settings->show();
    } else {
        automode_settings->show();
    }
}

void RTTD::on_actionLoadFile_triggered()
{
    QString file_name = QFileDialog::getOpenFileName(this, tr("Open File"), "/");
    ui->file_upload->setText("File loaded: " + file_name);
    my_pcap_file = file_name.toStdString();

    if (my_pcap_file == "") {
        QMessageBox::information(this, "File Upload", "File upload failed");
    } else {
        QMessageBox::information(this, "File Upload", "File has been successfully uploaded");
        pcap_t *descr;
        char errbuf[PCAP_ERRBUF_SIZE];

        descr = pcap_open_offline(my_pcap_file.c_str(), errbuf);
        if (descr == NULL) {
            QMessageBox::information(this, "Open Offline", "pcap_open_live() failed");
            qInfo() << "pcap_open_live() failed: " << errbuf << endl;
        } else {
            if (pcap_loop(descr, 0, packetHandler, NULL) < 0) {
                QMessageBox::information(this, "PCAP LOOP", "pcap_loop() failed");
                qInfo() << "pcap_loop() failed: " << pcap_geterr(descr);
            }
        }
    }
}

void RTTD::on_actionRun_triggered()
{

    if(is_pcap_mode) {
        ui->actionRun->setIcon(QIcon(":/run-green.png"));
        pcap_mode_timer->start();
        is_pcap_mode = false;
        automode.STATUS = true;
        qInfo() << "PCAP DATA: " << endl;
        emit this->sendSimulationValues(automode);
    } else {
        ui->actionRun->setIcon(QIcon(":/run.png"));
        pcap_mode_timer->stop();
        is_pcap_mode = true;
        automode.STATUS = false;
        emit this->sendSimulationValues(automode);
    }
}
