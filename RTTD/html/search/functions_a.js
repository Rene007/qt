var searchData=
[
  ['savereadingvalues',['SaveReadingValues',['../class_file.html#a72abb4e6a80260cd3bb746084838d837',1,'File']]],
  ['setconstants',['setConstants',['../class_plot.html#a2a430bbeaaf9dd5ec6854a545a6869f9',1,'Plot::setConstants(const ImpactConstants &amp;constants)'],['../class_plot.html#ab02c7d508e5af9fe9736a9e5e65737a8',1,'Plot::setConstants(const PDFConstants &amp;constants)']]],
  ['setthresholdlabel',['setThresholdLabel',['../class_plot.html#af6e28e7a9c40699fbaf15f60177a13cd',1,'Plot']]],
  ['settitle',['setTitle',['../class_plot.html#aa3bbab50a2e9ec87c3a67baa713babd6',1,'Plot']]],
  ['shifted_5fmean',['shifted_mean',['../class_core_utils.html#ad3e9abd9abe6213d2756afd46715ca85',1,'CoreUtils']]],
  ['standarddeviation',['StandardDeviation',['../class_observation.html#aeafe92e264ae0bb8dc28f94071cff12f',1,'Observation']]],
  ['std_5fdev',['std_dev',['../class_core_utils.html#a01be8d418168f898c8b888658611394c',1,'CoreUtils']]]
];
