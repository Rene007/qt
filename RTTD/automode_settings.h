#ifndef AUTOMODE_SETTINGS_H
#define AUTOMODE_SETTINGS_H

#include <QWidget>

namespace Ui {
class AutoMode_Settings;
}

class AutoMode_Settings : public QWidget
{
    Q_OBJECT

public:
    explicit AutoMode_Settings(QWidget *parent = 0);
    ~AutoMode_Settings();

public slots:
    void textEdit();
    void setValue(const double &min, const double &max, const double &multiplier);
    void setIAT(int);

signals:
    void valuesChanged(const QHash<QString, double> &params);

private slots:
    void on_apply_clicked();

private:
    Ui::AutoMode_Settings *ui;
};

#endif // AUTOMODE_SETTINGS_H
