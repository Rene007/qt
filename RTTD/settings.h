#ifndef SETTINGS_H
#define SETTINGS_H

#include <QWidget>
#include <QTextEdit>
#include <QHash>

namespace Ui
{
    class Settings;
}

class Settings : public QWidget
{
    Q_OBJECT

    public:
        explicit Settings(QWidget *parent = 0);
        ~Settings();

    public slots:
        void setValue(const QHash<QString, double> &params);
        void textEdit();

    signals:
        void valuesChanged(QHash<QString, double>, QHash<QString, bool>);

    private slots:
        void on_pushButton_clicked();
        void on_radioButton_std_dev_clicked();
        void on_radioButton_percentage_clicked();

private:
        Ui::Settings *ui;
        QHash<QString, double> params;
};

#endif // SETTINGS_H
