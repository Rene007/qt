#ifndef COREUTILS_H
#define COREUTILS_H

#include <QtMath>
#include <QVector>
#include <QWindow>
#include "configurations.h"

/**
 * @brief The CoreUtils class holds all
 * the core functions neeeded to manipulate
 * data
 * @author Rene Midouin
 * @version 1.0
 */
class CoreUtils
{
    static const ImpactConstants initial_impact;
    static const PDFConstants initial_pdf;

    public:
        /**
         * @brief normPDF calculates the probablity of P(x = val) using the Gaussian Probility Density Function
         * @param x The value we need to find probability for
         * @param mu The mean of the distribution
         * @param sigma The standard deviation of the distribution
         * @return Probability value of x, where 0 <= probability_value <= 1
         */
        static double normPDF(double x, double mu, double sigma);

        /**
         * @brief normCDF calculates the probablity of P(x <= val) using the Gaussian Probility Density Function
         * @param x The value we need to find probability to the left of x
         * @param mu The mean of the distribution
         * @param sigma The standard deviation of the distribution
         * @return Probability value of x, where 0 <= probability_value <= 1
         */
        static double normCDF(double x, double mu, double sigma);

        /**
         * @brief FPR calculates the False Positive Rate or (1 - specificity) of a test
         * @param FP False Positive
         * @param TN True Negative
         * @return The False Positive Rate
         */
        static double FPR(const double &FP, const double &TN);

        /**
         * @brief TPR calculates the True Positive Rate or the sensitivity of a test
         * @param TP True Positive
         * @param FN False Negative
         * @return The True Positive Rate
         */
        static double TPR(const double &TP, const double &FN);

        /**
         * @brief impactIntercept calculates the intersection between both impact curves
         * @param threshold The threshold value
         * @param impact_constants The Impact Constants, R1, R2, and LOSS (see @ref ImpactConstants)
         * @return The new threshold value after impact
         */
        static double impactIntercept(const double &threshold, const ImpactConstants &impact_constants = initial_impact);

        /**
         * @brief decisionFunction decides if a packet is considered as attack or not
         * @param p1 Usually this is P(s1|x), where P(s1|x) = [ P(x|s1) * P(s1)  ] / P(x)
         * @param p2 Usually this is P(s2|x), where P(s2|x) = [ P(x|s2) * P(s2)  ] / P(x)
         * @param isValidObservation A bool that is true as default (meaning new observation is valid),
         * false if new observation is not valid. See @ref isValidObservation
         * @return True if packet is considered as an attack, false otherwise
         */
        static bool decisionFunction(const double &p1, const double &p2, const double &observation, const double &optimal_threshold, const PDFConstants &pdf_constants);

        /**
         * @brief isValidObservation checks if a new observation is valid based on certain conditions
         * @param observation The new observation
         * @param mu The mean of the probability distribution
         * @param sigma The standard deviation of the probability distribution
         * @param pdf_constants The constants and restrictions for probability density function (see @ref PDFConstants)
         * @return True if new validation is considered valid based on our logic, false otherwise
         */
        static bool isValidObservation(const double &observation, const double &mu, const double &sigma, const PDFConstants &pdf_constants = initial_pdf);

        /**
         * @brief mean calculates the average value of a set of data
         * @param data vector of double holding the data
         * @return The average of the set of data
         */
        static double mean(const QVector<double>& data);

        /**
         * @brief std_dev calculates the standard deviation between a list of data
         * @param data vector of doubles holding the data
         * @return The standard deviation between a list of data
         */
        static double std_dev(const QVector<double>& data);

        /**
         * @brief variance calculates the variance between a list of data
         * @param data vector of doubles holding the data
         * @return The variance between a list of data
         */
        static double variance(const QVector<double>& data);

        /**
         * @brief shifted_mean calculates mean2, which is the mean shifted from original mean, of all PDF curves
         * @param mu The mean of the probability distribution
         * @param sigma The standard deviation of the distribution
         * @param pdf_constants The constants and restrictions for probability density function (see @ref PDFConstants)
         * @return
         */
        static double shifted_mean(const double &mu, const double &sigma, const PDFConstants &pdf_constants = initial_pdf);

private:
        CoreUtils();
};

#endif // COREUTILS_H
